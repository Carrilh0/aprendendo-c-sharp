﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            String Nome;

            Console.WriteLine("Digite o seu nome...");
            Nome = Console.ReadLine();

            /*  Forma alternativa para comparar string vazia ou nula
             
              String.IsNullOrEmpty(Nome);
             
             */
            if (Nome == String.Empty){
                Console.WriteLine("Nome não informado");
            }
            else{
                Console.WriteLine("Seu nome é " + Nome);
            }


            //Calculo 

            double numero1;
            double numero2;
            double soma;
            string operador;

            Console.WriteLine("Informe o operador");
            operador = Console.ReadLine();

            Console.WriteLine("Informe o primeiro numero");
            numero1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Informe o segundo numero");
            numero2 = Convert.ToDouble(Console.ReadLine());

            
           

            switch (operador)
            {
                case "+":
                    soma = (numero1 + numero2);
                break;
                
                case "-":
                    soma = (numero1 - numero2);
                break;

                default:
                    soma = 0;
                break;

            }
            
            Console.WriteLine("A soma dos numeros são: "+soma);

            //Calculo 2
            double numero;
            double total = 0;

            operador = "+";

            while (operador != "="){

                Console.WriteLine("Informe valor");
                numero = Convert.ToDouble(Console.ReadLine());

                

                switch (operador)
                {
                    case "+":
                        total += numero;
                        break;

                    case "-":
                        total -= numero;
                        break;

                    case "=":
                        break;


                }

                Console.WriteLine("Informe operador");
                operador = Console.ReadLine();

            }

            Console.WriteLine("Valor total: "+total);

            Console.ReadKey();
        }
    }
}
